Sequel.migration do
  change do
    create_table(:deposits) do
      primary_key :id, type: :Bignum
      Bignum      :amount, null: false
      foreign_key :transaction_id, index: true
      foreign_key :address_id, index: true
    end
  end
end
