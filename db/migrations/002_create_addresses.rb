Sequel.migration do
  change do
    create_table(:addresses) do
      primary_key :id, type: :Bignum
      String  :address, index: true
      Bignum  :received, default: 0
    end
  end
end
