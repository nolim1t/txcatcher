Sequel.migration do
  change do
    alter_table :transactions do
      add_column :created_at, DateTime
    end
  end
end
