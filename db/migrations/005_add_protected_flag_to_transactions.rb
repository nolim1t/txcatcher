Sequel.migration do
  change do
    alter_table :transactions do
      # This flag means someone has checked it once through the API and we might not want to remove
      # it from the DB (upon scheduled cleanup) because it's of a potential interest.
      add_column :protected, FalseClass, default: false
    end
  end
end
