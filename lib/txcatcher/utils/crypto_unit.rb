module TxCatcher

  class CryptoUnit
  
    def self.new(type, amount, *args)
      if type.to_s == "ltc" || type.to_s == "litecoin"
        return LitoshiUnit.new(amount, *args)
      else
        return SatoshiUnit.new(amount, *args)
      end
    end

  end

end
