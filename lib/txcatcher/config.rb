require 'ostruct'

module TxCatcher

  class << (Config = OpenStruct.new)
    def [](key_chain)
      key_chain = key_chain.to_s.split('.')
      config    = self.public_send(key_chain.shift)
      key_chain.each do |key|
        if config.kind_of?(Hash)
          config = config[key] || config[key.to_sym]
        else
          return
        end
      end
      config
    end
  end

end
